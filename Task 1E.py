#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 23 14:34:28 2017

@author: Steven
"""

from floodsystem.geo import rivers_by_station_number
from floodsystem.stationdata import build_station_list

print (rivers_by_station_number(build_station_list(),9))