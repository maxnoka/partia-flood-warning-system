"""Requirements for Task 1D"""

from floodsystem.geo import rivers_with_station, stations_by_river
from floodsystem.stationdata import build_station_list


print (sorted(rivers_with_station(build_station_list()))[:10])
print (sorted(stations_by_river(build_station_list())["River Aire"])) 
print (sorted(stations_by_river(build_station_list())["River Cam"])) 
print (sorted(stations_by_river(build_station_list())["Thames"])) 
   
    
    
