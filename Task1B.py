from floodsystem.geo import stations_by_distance
from floodsystem.stationdata import build_station_list

"""Requirements for Task 1A

Note also  list comprehension to replace the MonitoringStation object with its name
for easier printing"""

# Prints closest 10              
print ("Closest 10: ",[(station_distance_pair[0].name,station_distance_pair[1]) for station_distance_pair in stations_by_distance(build_station_list(),(52.2053, 0.1218))[:10]])

# Prints furthest 10
print ("Furthest 10: ",[(station_distance_pair[0].name,station_distance_pair[1]) for station_distance_pair in stations_by_distance(build_station_list(),(52.2053, 0.1218))[-10:]])
