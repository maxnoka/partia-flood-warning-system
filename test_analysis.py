"""Unit test for the analysis module"""

import pytest
from floodsystem.analysis import water_level_trends, flood_warnings
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.datafetcher import fetch_measure_levels
import random
import datetime
import numpy as np

def test_water_level_trends():
    """ Test analysis of water level trends"""
    stations = build_station_list()
    station = stations[2]
    
    dates, levels = fetch_measure_levels(station.measure_id,
                                        dt=datetime.timedelta(days=2))
    
    new_levels = -np.linspace(0,10,len(dates))
    
    assert water_level_trends(dates, new_levels, 4) == 'rising'
    
def test_flood_warnings():
    """ Test analysis of flood warnings"""
    stations = build_station_list()
    update_water_levels(stations)
    
    warnings = flood_warnings(stations)
    
    for warning in warnings:
        if warning[2] == 0:
            test_warning = warning
            break

    for station in stations:
        if station.name == test_warning[0]:
            test_station = station
    
    dates, levels = fetch_measure_levels(test_station.measure_id,
                                        dt=datetime.timedelta(days=2))
    
    assert water_level_trends(dates, levels, 8) == 'falling'