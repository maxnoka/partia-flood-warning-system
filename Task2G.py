"""Requirements for Task 2G"""

import pprint as pp
import datetime
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.analysis import flood_warnings
from floodsystem.plot import plot_water_levels

# get required data
stations = build_station_list()
update_water_levels(stations)

# get floodwarnings
flood_warnings_list = flood_warnings(stations)

pp.pprint(flood_warnings_list)

# plot one of the stations with the highest risk level seen
for station in stations:
    if station.name == flood_warnings_list[len(flood_warnings_list)-1][0]:
        riskiest_station = station

print(riskiest_station)
dates, levels = fetch_measure_levels(riskiest_station.measure_id,
                                        dt=datetime.timedelta(days=2))

plot_water_levels(riskiest_station, dates, levels)