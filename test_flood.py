""" Unit test for the flood module """

import pytest
import random
from floodsystem.flood import stations_level_over_threshold
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.station import inconsistent_typical_range_stations

def test_stations_level_over_threshold():
    stations = build_station_list()
    station = stations[2]
    update_water_levels([station])
    
    stations_over_threshold = stations_level_over_threshold([station], 1)
    if len(stations_over_threshold) == 1:
        assert station.latest_level > station.typical_range[1]
    else:
        assert station.latest_level < station.typical_range[1] or len(inconsistent_typical_range_stations([station]))==1

