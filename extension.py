"""
Just having some fun with Pyplot.
"""

import sys, getopt
from floodsystem.pyplot_extension import update_station_data, pyplot_generator, build_html

def main(argv):
    pyplots = []
    update = True
    coords = None
    
    # Parse Command Line Options
    try:
        opts, args = getopt.getopt(argv,"huc:", ["update", "coords", "help"])
    except getopt.GetoptError:
        print('extension.py -c(oordinates) -u(pdate) -h(elp)')
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print('extension.py -c(oordinates) -u(pdate) -h(elp)')
            sys.exit()
        if opt in ("-c", "coords"):
            coords = arg
        if opt in ("-u", "--update"):
            update = False
    
    if update == True:
        update_station_data(coords)
    
    if coords != None:
        pyplots.append(pyplot_generator(coords))
    # Create pyplot links
    pyplots.append(pyplot_generator())
    
    # Build the webpage with the given pyplots
    build_html(pyplots)
        

if __name__ == "__main__":
   main(sys.argv[1:])