"""Requirements for Task 1C"""

from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_within_radius

"""Note list comprehension to replace the MonitoringStation object with its name
for easier printing"""

# Prints all stations within 10km, sorted            
print ("Within 10km: ",sorted([station_distance_pair[0].name for station_distance_pair in stations_within_radius(build_station_list(),(52.2053, 0.1218),10)]))