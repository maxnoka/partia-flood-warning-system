#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb 26 12:07:04 2017

@author: Steven
"""

from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_highest_rel_level


def run():
    #imports station data
    stations = build_station_list()
    
    #updates water levels
    update_water_levels(stations)
    N= 10
    #gets list of risky stations
    RiskyObjectsStations= stations_highest_rel_level(stations,N)

    risky_stations = []
    #appends list with appropriate tuples
    for station_tuple in RiskyObjectsStations:
        s_name = station_tuple[0].name
        relWaterLevel = station_tuple[1]
        current_station = [(s_name, relWaterLevel)]
        risky_stations += current_station

    for station in risky_stations:
        print(station[0]+ "    " +str(station[1]))

if __name__ == "__main__":
    print("***Task2C: CUED Part IA Flood Warning System ***")
    
    run()
    
        
        

        

            
