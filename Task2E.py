"""Requirements for Task 2E"""

import datetime
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.flood import stations_level_over_threshold
from floodsystem.plot import plot_water_levels
from floodsystem.utils import sorted_by_key

# Get Data
stations = build_station_list()
update_water_levels(stations)

# build list of station water level tuples. make sure we get all by setting tol=0
station_water_level_tuples = stations_level_over_threshold(stations, 0)
# get those with top ten water levels and isolate the station objects
greatest_water_level_stations = [station_water_level_tuples[0] for station_water_level_tuples in sorted_by_key(station_water_level_tuples,1)[-5:]]  

# plot each station's data
for station in greatest_water_level_stations:
    dates, levels = fetch_measure_levels(station.measure_id,
                                        dt=datetime.timedelta(days=10))
    plot_water_levels(station, dates, levels)