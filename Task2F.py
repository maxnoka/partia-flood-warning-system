import datetime
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.flood import  stations_highest_rel_level
from floodsystem.plot import plot_water_levels, plot_water_level_with_fit
#gets station data
stations = build_station_list()
#updates water levels
update_water_levels(stations)
N=5
#gets list of risky stations
RiskyObjectsStations = stations_highest_rel_level(stations,N)
#fetches the data for the required number of days (2)
dt = 2
#sets the degree of fitting
polynomial_fitting = 4

for stationObjectTuple in RiskyObjectsStations:
    #gets current station object
    current_object = stationObjectTuple[0]
    #gets dates and corresponding levels
    dates, levels = fetch_measure_levels(current_object.measure_id, dt=datetime.timedelta(days=dt))
    if dates == []:
        continue
    #plots
    plot_water_level_with_fit(current_object,dates,levels, polynomial_fitting)
    



