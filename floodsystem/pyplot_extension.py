"""This module contains a collection of functions related to
the Pyplot and building some HTML.

"""

import pickle
import plotly as py
import plotly.graph_objs as go

import datetime

from floodsystem.utils import save_object, sorted_by_key

from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.analysis import flood_warnings, water_level_trends
from floodsystem.geo import stations_by_distance

def pyplot_generator(*args):
    """
    Takes optionally coordinates and creates a Pyplot map, and returns the path
    to the file. If no coordinates are given, this will be an overview map,
    otherwise it focusses on the given location.
    """
    
    # Load raw station data
    with open('raw_stations.pkl', 'rb') as input:
        raw_stations = pickle.load(input)
    
    station_labels = []
    colors = ['rgb(106, 187, 35)', 'rgb(254, 214, 50)', 'rgb(253, 105, 34)', 
              'rgb(252, 17, 27)', 'rgb(229, 229, 229)']
    
    # Parameters for overview map
    custom_lon = dict( range= [ -6.000, 1.600 ] )
    custom_lat = dict( range= [ 50.600, 55.500] )
    file_name = 'Stations_and_Flood_Risk_UK.html'
    title = 'Overview of Monitoring Stations'
    
    # Overwrites the above when coordinates are given. Also adds a marker for 
    # the given location.
    if len(args) == 1 and args[0]!=None:
        arg = args[0]
        coords = eval(arg)
        title = 'Your Current Location'
        file_name = 'Stations_and_Flood_Risk_UK_2.html'
        custom_lon = dict( range =[coords[1] - 0.1, coords[1] + 0.1])
        custom_lat = dict( range =[coords[0] - 0.1, coords[0] + 0.1])
        # Create station label objects as plotly data
        station_labels.append(go.Scattergeo(
            lon = [coords[1]],
            lat = [coords[0]],
            name = "Your Location",
            opacity = 1,
            mode = 'markers',
            showlegend = False,
            geo = 'geo',
            marker = dict(
                    size = 20,
                    color = 'rgb(255, 255, 255)',
                    line = dict(width=0.5, color='rgb(40,40,40)'),
            )
        ))
    
    # Create station labels
    for station in raw_stations:
        # Get appropriate sizes for the balloons
        if station.risk_level == 4:
            size = 4
            opacity = 0.5
        else:
            size = 6 + station.risk_level * 2
            opacity = 1
        
        # Create station label objects as plotly data
        station_labels.append(go.Scattergeo(
            lon = [station.coord[1]],
            lat = [station.coord[0]],
            name = station.risk_level,
            text = "<b>{0}</b><br>Current Water Level: {1} <br>Typical Range: {2} '<br>Trend: {3}".format(
                station.name, station.latest_level, station.typical_range, station.water_level_trend),
            opacity = opacity,
            mode = 'markers',
            showlegend = False,
            geo = 'geo',
            marker = dict(
                    size = size,
                    color = colors[station.risk_level],
                    line = dict(width=0.5, color='rgb(40,40,40)'),
            )
        ))
    
    # Create plotly layout, centred on England/given coordinates        
    layout = go.Layout(
        title = title,
        geo = dict(
            resolution = 50,
            scope = 'europe',
            showframe = False,
            showcoastlines = True,
            showland = True,
            landcolor = "rgb(229, 229, 229)",
            countrycolor = "rgb(255, 255, 255)" ,
            coastlinecolor = "rgb(255, 255, 255)",
            showsubunits = True,
            projection = dict(
                type = 'Mercator'
            ),
            lonaxis = custom_lon,
            lataxis = custom_lat,
            domain = dict(
                x = [ 0, 1 ],
                y = [ 0, 1 ]
            )
        ),
        autosize=True
    )
    
    # Create figure, create plotly plot in offline mode, returning the location of that file
    fig = go.Figure(layout=layout, data=station_labels)
    return py.offline.plot(fig, validate=False, filename=file_name, auto_open=False)

def update_station_data(*args):
    """
    Updates station data. Saves firstly: high risk stations with associated data.
    Secondly (if coordinates are given): Nearby stations with risk data and distance 
    from given coordinates
    """
    # Build station Llists with water levels
    raw_stations = build_station_list()
    update_water_levels(raw_stations)
    # Get Flood Warnings
    flood_warnings_list = flood_warnings(raw_stations)
    
    high_risk_stations = []
    
    # Add risk_levels and trend data to station objects (just a consequence of the
    # function signatures of the exercises), also seperately save high risk stations
    for station in raw_stations:
        # if no warning exists, risk_level is v. low
        station.risk_level = 4
        for warning in flood_warnings_list:
            if station.name == warning[0]:
                station.risk_level = warning[2]
                dates, levels = fetch_measure_levels(station.measure_id, 
                                                     dt=datetime.timedelta(days=2))
                # ignore nonsense data
                if dates != []:
                    station.water_level_trend = water_level_trends(dates, levels, 8)
                high_risk_stations.append((station, station.risk_level))
                    
    save_object(raw_stations, 'raw_stations.pkl')
    save_object(sorted_by_key(high_risk_stations,1), 'high_risk_stations.pkl')

    # if coords are given, get nearby stations, check if they have an associated
    # risk level and save
    if len(args) == 1 and args[0]!=None:
        nearby_stations = stations_by_distance(raw_stations, eval(args[0]))[:10]
        for station in nearby_stations:
            for high_risk_station in high_risk_stations:
                if station[0].name == high_risk_station[0].name:
                    station[0].risk_level = high_risk_station[0].risk_level
        save_object(nearby_stations, 'nearby_stations.pkl')


def build_html(plot_url):
    """
    Creates the dynamic parts of the webpage. If more than one plot is given, 
    the first one is assumed to be the zoomed in one.
    """     
    # Get High Risk Stations
    with open('high_risk_stations.pkl', 'rb') as input:
        high_risk_stations = pickle.load(input)
    
    risk_level = ["low", "medium", "high", "severe", "v. low"]
    link = ''
    
    # if more than one plot is given, means that there is a zoomed in view
    if len(plot_url) > 1:
        with open('nearby_stations.pkl', 'rb') as input:
            nearby_stations = pickle.load(input)
        link = '''
        <br><a href="./overview.html">View overview.</a>
        '''
        
        # get the highest risk among the nearby stations
        risks_near_you = []
        for station in nearby_stations:
            risks_near_you.append(station[0].risk_level)
            
        content = '''<h3>Risk Near You Is:</h3><br><p class="risk_level '''+risk_level[sorted(risks_near_you)[0]]+'''">'''+risk_level[sorted(risks_near_you)[0]]+'''</p><h3>Stations Near You:</h3><br><table><thead><tr><th>Station</th><th>Risk Level</th><th>Distance (km)</th></thead><tbody>'''
        for station in nearby_stations:
            content = content + "<tr class=\""+risk_level[station[0].risk_level]+"\"><th>"+station[0].name+"</th><th>"+risk_level[station[0].risk_level]+"</th><th>"+'{0:.2f}'.format(station[1])+"</th></li>"
        content = content + "</tbody></table>"
        
        fill_in('./your_location.html', content, plot_url[0], link)
        link = '''
        <br><a href="./your_location.html">View your own location.</a>
        '''
    
    # Same thing as above, but for the overview
    content = "<h3>Current Stations of Import</h3> <br><table><thead><tr><th>Station</th><th>Risk Level</th><th>Trend</th></thead><tbody>"
    for station in high_risk_stations:
        content = content + "<tr class=\""+risk_level[station[1]]+"\"><th>"+station[0].name+"</th><th>"+risk_level[station[1]]+"</th><th>"+station[0].water_level_trend+"</th></li>"
    content = content + "</tbody></table>"
        
    if len(plot_url) > 1:
        fill_in('./overview.html', content, plot_url[1], link)
    else:
        fill_in('./overview.html', content, plot_url[0], link)
    
def fill_in(file_name, content, plot_url, link):
    """
    Takes in the dynamic parts and then assembles and saves the html.
    """
    html_string = '''
<html>
	<head>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
		<title>England Flood Monitoring Stations and Associated Flood Risk</title>
		<style>
		body{ margin:0 100; background:whitesmoke; }

		div.container {
			width: 100%;
		}

		#header {
			padding: 1em;
			clear: left;
			text-align: center;
		}
          
           .risk_level {
                font-weight: bold;
                text-transform: uppercase;
                font-size: 250%;
           }
  
           .severe {
                color: rgb(252, 17, 27) 
           }
           
           .high {
                color: rgb(253, 105, 34) 
           }
           
           .medium {
                color: rgb(254, 214, 50) 
           }
           
           .low {
                color: rgb(106, 187, 35) 
           }

		#side_text {
			float: left;
			max-width: 560px;
			margin: 0;
			padding: 1em;
			height: 100%;
			background-color: white;
			margin-top: 22;
		}
  
          th {
              font-weight: normal;
              padding-right: 10;
          }
          
          thead th {
                font-weight: bold;
                padding-bottom: 10px;
          }
  
		#plot {
			margin-left: 170px;
			padding: 1em;
			overflow: hidden;
		}
		</style>
	</head>
	<body>
		<div id="header">    
			<h1>UK Water Level Monitoring Stations and Flood Risk</h1>
		</div>
		<div class="container">
			<div class="content">
                      <div id="side_text">
                           ''' + content + link + '''
                      </div>
				<div id="plot">
					<iframe width="100%" height="100%" frameborder="0" seamless="seamless" scrolling="no" \
     src="''' + plot_url + '''?"></iframe>
				</div>
			</div>
			<p>Created as part of the Cambridge Engineering IA Computing Course.</p>
		</div>
	</body>
</html>
'''
    
    f = open(file_name,'w')
    f.write(html_string)
    f.close()   
