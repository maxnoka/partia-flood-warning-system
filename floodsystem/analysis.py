
import matplotlib
import matplotlib.dates
import datetime
import numpy as np
from floodsystem.flood import stations_level_over_threshold, stations_highest_rel_level
from floodsystem.datafetcher import fetch_measure_levels
from .utils import sorted_by_key 

def polyfit(dates, levels, p):
    dates_floats = matplotlib.dates.date2num(dates)
    x = dates_floats
    y = levels
    
    p_coeff = np.polyfit(x -x[0], y,p)
    
    # Convert coefficient into a polynomial that can be evaluated
    poly = np.poly1d(p_coeff)
    
    return poly, x[0], dates_floats

def water_level_trends(dates, levels, p):
    """ Checks the gradient of the best fit line to the data to check for
    rising/falling/little change in the trend"""
    
    f,shift, dates_floats  = polyfit(dates, levels, p)
    current_grad = f.deriv()(0)
    if current_grad > 0.5:
        return 'rising'
    elif current_grad < -0.5:
        return 'falling'
    else:
        return 'little change'
        
def flood_warnings(stations):
    """ create a list of stations with highest flood risk. this is based on the
    relative water level and the water level trend"""
    
    # create list of stations with high and very high water levels (based on threshold 
    # relative water levels)
    number_high_very_high = len(stations_level_over_threshold(stations, 1.3))
    number_very_high = len(stations_level_over_threshold(stations, 1.7))
    greatest_water_level_stations = [station_water_level_tuples[0] for station_water_level_tuples in sorted_by_key(stations_level_over_threshold(stations, 0),1)[-number_high_very_high:]] 
    very_high_water_level = greatest_water_level_stations[-number_very_high:]
    
    risk_evaluation = []
    
    # build up list of these stations based on water level trend (refer to threat
    # matrix)
    for station in greatest_water_level_stations:
        dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=2))
        if dates == []:
            continue
        water_level_trend = water_level_trends(dates, levels, 8)
        
        if station in very_high_water_level:
            if water_level_trend == 'rising':
                risk = (3,'severe')
            elif water_level_trend == 'little change':
                risk = (2,'high')
            elif water_level_trend == 'falling':
                risk = (1,'moderate')
        else:
            if water_level_trend == 'rising':
                risk = (2,'high')
            elif water_level_trend == 'little change':
                risk = (1,'moderate')
            elif water_level_trend == 'falling':
                risk = (0,'low')       
    
        risk_evaluation.append((station.name, risk[1],risk[0]))
    return sorted_by_key(risk_evaluation,2)
