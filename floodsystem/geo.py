"""This module contains a collection of functions related to
geographical data.

"""

from .utils import sorted_by_key
from haversine import haversine
from floodsystem.stationdata import build_station_list


def stations_by_distance(stations, p):
    """ Given a coordinate p, function returns a sorted list (station,distance)
    of the stations wih their distances from p"""
    
    station_distances = []
    
    # Iterate through stations list
    for station in stations:
        distance = haversine(p,station.coord,miles=False)
        station_distances.append((station,distance))
        
    return sorted_by_key(station_distances,1)
    
def rivers_with_station(stations):
    """ Returns a set of all rivers that have a monitoring station"""
    station_in_rivers = set()
    for station in stations:
        station_in_rivers.add(station.river)

    return (station_in_rivers)
    
def stations_by_river(stations):
    """Returns a dictionary with stations assigned to rivers"""
    #sets a dictionary with the river names as the keys and uses previous function
    river_plus_stations=dict.fromkeys(rivers_with_station(stations))
    for key in river_plus_stations:
        #loop that assigns the values as empty lists
        river_plus_stations[key] =[] 
    for station in stations:
        #assigns the stations to the empty lists that correspond to their correct river
        river = station.river
        river_plus_stations[river].append(station.name)
    return river_plus_stations
    
def stations_within_radius(stations, centre, r):
    """ Returns a list of stations (MonitoringSations objects) within a 
    radius of r of the coordinate centre"""
    
    station_distances = stations_by_distance(build_station_list(), centre)
    stations_within_distance = []
    
    for station_distance_pair in station_distances:
        if station_distance_pair[1] <= r:
            stations_within_distance.append(station_distance_pair)
            
    return stations_within_distance
    
def rivers_by_station_number(stations,N):
    """ Returns the N rivers with most stations, in form of (river, # of stations)
    tuples. Rivers with the same number of station as the Nth entry are included """
    river_plus_stations = stations_by_river(stations)
    river_plus_number_stations = []
    for river in river_plus_stations:
        number_stations = len(river_plus_stations[river])
        tuple_river_number = (river,number_stations)
        river_plus_number_stations.append(tuple_river_number)
    sorted_river_plus_number_stations=sorted_by_key(river_plus_number_stations,1)
    min_number_stations = sorted_river_plus_number_stations[-N][1]
    repeated_stations = []
    for river_tuple in sorted_river_plus_number_stations:
        if river_tuple[1] == min_number_stations:
            repeated_stations.append(river_tuple)
    merged_station_list = repeated_stations + sorted_river_plus_number_stations[-(N-1):]
    return  merged_station_list
        

        