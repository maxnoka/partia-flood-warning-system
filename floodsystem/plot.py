""" This module contains functions related to plotting flood relevant data
from stations. """

import matplotlib.pyplot as plt
from datetime import datetime, timedelta

from .station import MonitoringStation
from .datafetcher import fetch_measure_levels
from .analysis import polyfit

def plot_water_levels(station, dates, levels):
    """
    displays a plot of the water level data against time for a station
    including plot lines for the typical low and high levels.
    """
    
    # Plot
    plt.plot(dates, levels)
    
    # Add axis labels, rotate date labels and add plot title
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.axhline(station.typical_range[0], color='r', label='Lower', linestyle='--')
    plt.axhline(station.typical_range[1], color='b', label='Upper', linestyle='--')
    plt.xticks(rotation=45);
    plt.title(station.name)
    plt.legend(loc='lower left')
    
    # Display plot
    plt.tight_layout()  # This makes sure plot does not cut off date labels
    plt.show()
    
def plot_water_level_with_fit(station,dates,levels,p):
    polynomial,shift, dates_floats = polyfit(dates, levels, p)
    plt.plot(dates_floats-shift, levels)
    plt.plot((dates_floats-shift), polynomial(dates_floats-shift))
    
    # Add axis labels, rotate date labels and add plot title
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.axhline(station.typical_range[0], color='r', label='Lower', linestyle='--')
    plt.axhline(station.typical_range[1], color='b', label='Upper', linestyle='--')
    plt.xticks(rotation=45);
    plt.title(station.name)
    plt.legend(loc='lower left')
    
    plt.show()