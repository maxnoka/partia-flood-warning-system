""" This module contains functions related to processing flood relevant data
from stations. """

from .station import MonitoringStation
from .utils import sorted_by_key

def stations_level_over_threshold(stations, tol):
    """ returns a list of station, relative water level tuples for which the
    relative water level is greater than tol (descending order) """ 
    stations_over_threshold = []
    for station in stations:
        if station.relative_water_level() != None and station.relative_water_level() > tol:
            stations_over_threshold.append((station, station.relative_water_level()))
    return sorted_by_key(stations_over_threshold,1)
    
def stations_highest_rel_level(stations,N):
    """ returns a list of the N stations of which the water level is the highest"""
    #uses above function but with very low tolerance
    tol= 0.0
    #creates a list of all the stations at risk
    stations_over_threshold = stations_level_over_threshold(stations, tol)
    #Gets the first N stations
    stations_over_threshold = stations_over_threshold [-N:]
    return stations_over_threshold
    