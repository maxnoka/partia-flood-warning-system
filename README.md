# CUED Part IA Flood Warning System

This directory contains our Flood Warning System, as specified by the requirements at
http://cued-partia-flood-warning.readthedocs.io/en/latest/index.html. In addition,
we have added a simple web-based user interface.

Because we did not have enough time, we did not implement a full MVC web-server
based system with a more robust templating engine. Instead, we opted to make it
command-line based, whereby static HTML is then saved to this directory.


### Usage

``` extension.py -c(oordinates) -u(pdate) -h(elp) ```

e.g. `extension.py "(52.2053, 0.1218)" -u`

Output is then one or two files: 'overview.html' and 'your_location.html'. Open 
either of these in a browser and enjoy :^)

-c is optional. If no coordinates are given, only an overview map is generated.

IMPORTANT: -u disables station data updates. By default station data
is updated every time the program is run. This can be turned off to save time, but
might mean the program fails if there is no initial data and will behave unexpectedly
if the coordinates are changed but the station data is not also updated.

### Details

The main additions to the minimum requirements is extension.py and the functions
implemented in the pyplot_extension module. There are three main parts:

1. update_station_data(), which gets flood warnings and optionally also nearby stations
and their associated risks. These are then saved to raw_stations.pkl and nearby_stations.pkl.

2. pyplot_generator(), which uses the [plot.ly](https://plot.ly/python/) to generate maps,
which are saved as their own html frames. There is a fair ammount of processing to get the
stations into a format that can be used as labels on the plot.ly map.

3. build_html() and fill_in(), which get relevant info and embed it into html.

### If it doesn't work

Send mn461@cam.ac.uk an email. In the meantime, also note backup/overview.html to see what it should look like.