"""Unit test for the geo module"""

import pytest
from floodsystem.geo import stations_by_distance, stations_within_radius,stations_by_river,rivers_by_station_number, rivers_with_station
from floodsystem.stationdata import build_station_list
import random


def test_stations_by_distance():
    """ Test building of distance lists"""
    stations = build_station_list()
    station_distances = stations_by_distance(stations, (52.2053, 0.1218))
    assert len(station_distances) == len(stations)
    
def test_stations_within_radius():
    """ Test creation of stations within radius lists"""
    stations = build_station_list()
    radius = random.randint(1,len(stations))
    for station_distance_pair in stations_within_radius(stations,(52.2053, 0.1218),radius):
        assert station_distance_pair[1] < radius

def test_stations_by_river():
    """ Test building of river to station list dictionary, asserting that if
    a station is on a given river, it should be in the corresponding list """
    stations = build_station_list()
    river_plus_stations = stations_by_river(stations)
    test_station = stations[56]
    assert test_station.name in river_plus_stations[test_station.river]

def test_rivers_by_station_number():
    """ Test creation of list of rivers and number of stations tuples. Asserting
    that the river with the most stations should be the Thames """
    stations = build_station_list()
    merged_list = rivers_by_station_number(stations,1)
    assert merged_list[0][0] == "Thames"
    
def test_rivers_with_station():
    """ Test building of list of all rivers with a station. Asserting that the
    river of a station should be in the list, by definition """
    stations = build_station_list()
    test_station = stations[56]
    assert test_station.river in rivers_with_station(stations)
