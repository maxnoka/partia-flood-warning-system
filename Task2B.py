"""Requirements for Task 2B"""

from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_level_over_threshold

stations = build_station_list()
update_water_levels(stations)

print([(station_threshold_pair[0].name,station_threshold_pair[1]) for station_threshold_pair in stations_level_over_threshold(stations,0.8)])